import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RoutingModule } from './routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SignupComponent } from './signup/signup.component';
import { SigninComponent } from './signin/signin.component';
import { MeteoComponent } from './meteo/meteo.component';
import { TraficComponent } from './trafic/trafic.component';
import { EventComponent } from './event/event.component';
import { EventCreateComponent } from './event-create/event-create.component';
import { EventEditComponent } from './event-edit/event-edit.component';
import { EventHomeComponent } from './event-home/event-home.component';
import { EventCheckComponent } from './event-check/event-check.component';
import { FirstAidComponent } from './first-aid/first-aid.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SignupComponent,
    SigninComponent,
    MeteoComponent,
    TraficComponent,
    EventComponent,
    EventCreateComponent,
    EventEditComponent,
    EventHomeComponent,
    EventCheckComponent,
    FirstAidComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
